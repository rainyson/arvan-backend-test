<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('discount_codes', function (Blueprint $table) {
            $table->id();
            $table->foreignId('creator_id')->nullable()
                ->constrained('managers');
            $table->foreignId('updater_id')->nullable()
                ->constrained('managers');
            $table->string('title');
            $table->string('code',10)
                ->unique()
                ->index();
            $table->unsignedInteger('amount');
            $table->unsignedBigInteger('use_count')->nullable();
            $table->unsignedBigInteger('used_count')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('discount_codes');
    }
};
