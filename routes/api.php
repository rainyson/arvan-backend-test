<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\V1\Manager\DiscountCodeController;
use App\Http\Controllers\Api\V1\User\{
    DiscountCodeController as UserDiscountCodeController,
    WalletController,
};

Route::prefix('v1')->name('v1.')->group(function () {
    Route::prefix('manager')->name('manager.')->group(function () {
        Route::prefix('discountCodes')->name('discountCodes.')->group(function () {
            Route::post('/', [DiscountCodeController::class, 'store'])->name('store');
            Route::get('/users', [DiscountCodeController::class, 'getUsers'])->name('users');
        });
    });
    Route::prefix('user')->name('user.')->group(function () {
        Route::post('discountCodes', [UserDiscountCodeController::class, 'store']);
        Route::prefix('wallets')->name('wallets')->group(function () {
            Route::get('/', [WalletController::class, 'index'])
                ->middleware('auth:sanctum')
                ->name('index');
            Route::get('/balance', [WalletController::class, 'getBalance'])
                ->middleware('auth:sanctum')
                ->name('balance');
        });
    });
});
