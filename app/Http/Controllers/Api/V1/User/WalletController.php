<?php

namespace App\Http\Controllers\Api\V1\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\Wallet\WalletResource;
use App\Interfaces\IWalletRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Auth;

class WalletController extends Controller
{
    private IWalletRepository $walletRepository;

    /**
     * instantiate class dependencies
     *
     * @param IWalletRepository $walletRepository
     */
    public function __construct(IWalletRepository $walletRepository)
    {
        $this->walletRepository = $walletRepository;
    }

    /**
     * Get user wallet transactions
     *
     * @return AnonymousResourceCollection
     */
    public function index(): AnonymousResourceCollection
    {
        return WalletResource::collection($this->walletRepository->getListByUser(Auth::user()->id));
    }

    /**
     * Get user wallet balance
     *
     * @return JsonResponse
     */
    public function getBalance(): JsonResponse
    {
        $user = Auth::user();
        return response()->json([
            'balance' => $user->wallet
        ]);
    }

}
