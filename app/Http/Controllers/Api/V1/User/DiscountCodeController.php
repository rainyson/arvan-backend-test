<?php

namespace App\Http\Controllers\Api\V1\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\DiscountCodeRequest;
use App\Http\Resources\DiscountCode\DiscountCodeResource;
use App\Interfaces\IDiscountCodeRepository;
use App\Interfaces\IUserAccessTokenRepository;
use App\Interfaces\IUserRepository;
use App\Interfaces\IWalletRepository;
use Illuminate\Http\Request;

class DiscountCodeController extends Controller
{
    private IDiscountCodeRepository $discountCodeRepository;
    private IUserRepository $userRepository;
    private IWalletRepository $walletRepository;
    private IUserAccessTokenRepository $userAccessTokenRepository;

    /**
     * instantiate class dependencies
     *
     * @param IDiscountCodeRepository $discountCodeRepository
     * @param IUserRepository $userRepository
     * @param IWalletRepository $walletRepository
     * @param IUserAccessTokenRepository $userAccessTokenRepository
     */
    public function __construct(IDiscountCodeRepository    $discountCodeRepository,
                                IUserRepository            $userRepository,
                                IWalletRepository          $walletRepository,
                                IUserAccessTokenRepository $userAccessTokenRepository)
    {
        $this->discountCodeRepository = $discountCodeRepository;
        $this->userRepository = $userRepository;
        $this->walletRepository = $walletRepository;
        $this->userAccessTokenRepository = $userAccessTokenRepository;
    }

    /**
     * This method retrieve specific discount code, check its validity, attach to user,
     * create wallet transaction and modify user wallet finally return discount code resource
     *
     * @param DiscountCodeRequest $request
     * @return array
     */
    public function store(DiscountCodeRequest $request): array
    {
        $validated = $request->validated();
        $user = $this->userRepository->findOrCreateByMobile($validated['mobile']);
        $token = $this->userAccessTokenRepository->createUserToken($user);
        $dc = $this->discountCodeRepository->getByCodeWithUserCount($validated['code'], $user->id);
        if (!$dc) {
            abort(404, __('messages.discountCode.wrongCode'));
        }
        if ($dc->users_count > 0) {
            abort(403, __('messages.discountCode.used'));
        }
        if ($dc->used_count >= $dc->use_count) {
            abort(403, __('messages.discountCode.useCountExceeded'));
        }
        $this->discountCodeRepository->attachCodeToUser($dc, $user->id);
        $this->discountCodeRepository->incrementCodeUsedCount($dc);
        $this->walletRepository->createWalletTransaction([
            'discountCodeId' => $dc->id,
            'amount' => $dc->amount,
            'reason' => 1
        ], $user->id);
        $this->userRepository->modifyWallet($user, $dc->amount);
        return array_merge((new DiscountCodeResource($dc))->toArray($request), [
            '_token' => $token
        ]);
    }
}
