<?php

namespace App\Http\Controllers\Api\V1\Manager;

use App\Http\Controllers\Controller;
use App\Http\Requests\Manager\DiscountCodeRequest;
use App\Http\Resources\DiscountCode\DiscountCodeResource;
use App\Http\Resources\User\UserResource;
use App\Interfaces\IDiscountCodeRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class DiscountCodeController extends Controller
{
    private IDiscountCodeRepository $discountCodeRepository;

    /**
     * instantiate class dependencies
     *
     * @param IDiscountCodeRepository $discountCodeRepository
     */
    public function __construct(IDiscountCodeRepository $discountCodeRepository)
    {
        $this->discountCodeRepository = $discountCodeRepository;
    }

    /**
     * Store a discount code into db
     *
     * @param DiscountCodeRequest $request
     * @return DiscountCodeResource
     */
    public function store(DiscountCodeRequest $request): DiscountCodeResource
    {
        $validated = $request->validated();
        return new DiscountCodeResource($this->discountCodeRepository->createDiscountCode($validated));
    }

    /**
     * Get list of users that used specific discount code
     *
     * @return AnonymousResourceCollection
     */
    public function getUsers(): AnonymousResourceCollection
    {
        return UserResource::collection($this->discountCodeRepository->getCodeUsers());
    }
}
