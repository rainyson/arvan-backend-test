<?php

namespace App\Http\Resources\Wallet;

use App\Http\Resources\DiscountCode\DiscountCodeResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class WalletResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'userId' => $this->user_id,
            'discountCodeId' => $this->discount_code_id,
            'amount' => $this->transaction_type,
            'transactionType' => $this->transaction_type,
            'reason' => $this->reason,
            // relationships
            'discountCode' => new DiscountCodeResource($this->whenLoaded('discountCode')),
            'createdAt' => $this->created_at,
            'updatedAt' => $this->updated_at
        ];
    }
}
