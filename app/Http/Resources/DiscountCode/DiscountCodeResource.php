<?php

namespace App\Http\Resources\DiscountCode;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class DiscountCodeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'creatorId' => $this->creator_id,
            'updaterId' => $this->updater_id,
            'title' => $this->title,
            'code' => $this->code,
            'amount' => $this->amount,
            'useCount' => $this->use_count,
            'usedCount' => $this->used_count,
            'createdAt' => $this->created_at,
            'updatedAt' => $this->updated_at,
        ];
    }
}
