<?php

namespace App\Repositories;

use App\Interfaces\IWalletRepository;
use App\Models\Wallet;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class WalletRepository implements IWalletRepository
{
    /**
     * Create Wallet transaction
     *
     * @param array $data
     * @param int $userId
     * @return Wallet
     */
    public function createWalletTransaction(array $data, int $userId): Wallet
    {
        return Wallet::create([
            'user_id' => $userId,
            'discount_code_id' => $data['discountCodeId'],
            'amount' => $data['amount'],
            'transaction_type' => $data['transactionType'] ?? 1,
            'reason' => $data['reason']
        ]);
    }

    /**
     * Get list of transactions by user id
     *
     * @param int $userId
     * @return LengthAwarePaginator
     */
    public function getListByUser(int $userId): LengthAwarePaginator
    {
        $page = request()->query('page');
        $pageNumber = $page['number'] ?? 1;
        $pageSize = $page['size'] ?? 10;
        return Wallet::with('discountCode')
            ->where('user_id', $userId)
            ->paginate($pageSize, ['*'], 'page', $pageNumber);
    }
}
