<?php

namespace App\Repositories;

use App\Interfaces\IUserRepository;
use App\Models\User;

class UserRepository implements IUserRepository
{
    /**
     * Find user by mobile and create if not exist
     *
     * @param string $mobile
     * @return User
     */
    public function findOrCreateByMobile(string $mobile): User
    {
        return User::firstOrCreate([
            'mobile' => $mobile
        ]);
    }

    /**
     * Increase or Decrease user wallet based on type
     *
     * @param User $user
     * @param int $amount
     * @param int $type
     * @return bool|int
     */
    public function modifyWallet(User $user, int $amount, int $type = 1): bool|int
    {
        return $user->increment('wallet', $type * $amount);
    }
}
