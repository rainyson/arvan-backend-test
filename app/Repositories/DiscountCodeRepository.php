<?php

namespace App\Repositories;

use App\Interfaces\IDiscountCodeRepository;
use App\Models\DiscountCode;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class DiscountCodeRepository implements IDiscountCodeRepository
{

    /**
     * Create new discount code
     *
     * @param array $data
     * @return DiscountCode
     */
    public function createDiscountCode(array $data): DiscountCode
    {
        return DiscountCode::create([
            'title' => $data['title'],
            'code' => $data['code'],
            'amount' => $data['amount'],
            'use_count' => $data['useCount'] ?? 1,
        ]);
    }

    /**
     * Find Discount code by code associated with count of specific user
     *
     * @param string $code
     * @param int $userId
     * @return DiscountCode|null
     */
    public function getByCodeWithUserCount(string $code, int $userId): DiscountCode|null
    {
        return DiscountCode::withCount([
            'users' => function (Builder $query) use ($userId) {
                $query->where('id', $userId);
            }
        ])->firstWhere('code', $code);
    }

    /**
     * Attach specific discount code(by code) to specific user
     *
     * @param DiscountCode|string $discountCode
     * @param int $userId
     * @return void
     */
    public function attachCodeToUser(DiscountCode|string $discountCode, int $userId): void
    {
        $discountCode = is_string($discountCode)
            ? DiscountCode::where('code', $discountCode)->firstOrFail()
            : $discountCode;
        $discountCode->users()->attach($userId);
    }

    /**
     * increment discount code used_count field by amount
     *
     * @param DiscountCode $discountCode
     * @param int $amount
     * @return bool|int
     */
    public function incrementCodeUsedCount(DiscountCode $discountCode, int $amount = 1): bool|int
    {
        return $discountCode->increment('used_count', $amount);
    }

    /**
     * Get list of users that used specific code
     *
     * @return LengthAwarePaginator
     */
    public function getCodeUsers(): LengthAwarePaginator
    {
        $dc = request()->input('filter.code');
        $page = request()->query('page');
        $pageNumber = $page['number'] ?? 1;
        $pageSize = $page['size'] ?? 10;
        if (!$dc) {
            abort(403);
        }
        return User::whereHas('discountCodes', function (Builder $query) use ($dc) {
            $query->where('code', $dc);
        })
            ->paginate($pageSize, ['*'], 'page', $pageNumber);
    }
}
