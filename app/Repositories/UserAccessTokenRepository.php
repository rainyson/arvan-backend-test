<?php

namespace App\Repositories;

use App\Interfaces\IUserAccessTokenRepository;
use App\Models\User;

class UserAccessTokenRepository implements IUserAccessTokenRepository
{
    /**
     * Delete all previous user tokens and create new user token
     *
     * @param User $user
     * @return string
     */
    public function createUserToken(User $user): string
    {
        $user->tokens()->delete();
        $token = $user->createToken($user->mobile);
        return $token->plainTextToken;
    }
}
