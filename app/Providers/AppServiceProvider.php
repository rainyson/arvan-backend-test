<?php

namespace App\Providers;

use App\Interfaces\IDiscountCodeRepository;
use App\Interfaces\IUserAccessTokenRepository;
use App\Interfaces\IUserRepository;
use App\Interfaces\IWalletRepository;
use App\Repositories\DiscountCodeRepository;
use App\Repositories\UserAccessTokenRepository;
use App\Repositories\UserRepository;
use App\Repositories\WalletRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(IDiscountCodeRepository::class, DiscountCodeRepository::class);
        $this->app->bind(IUserRepository::class, UserRepository::class);
        $this->app->bind(IWalletRepository::class, WalletRepository::class);
        $this->app->bind(IUserAccessTokenRepository::class, UserAccessTokenRepository::class);
        $this->app->bind(IWalletRepository::class, WalletRepository::class);
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {

    }
}
