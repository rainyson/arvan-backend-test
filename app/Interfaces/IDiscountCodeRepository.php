<?php

namespace App\Interfaces;

use App\Models\DiscountCode;
use Illuminate\Pagination\LengthAwarePaginator;

interface IDiscountCodeRepository
{
    public function createDiscountCode(array $data): DiscountCode;
    public function getByCodeWithUserCount(string $code, int $userId): DiscountCode|null;
    public function attachCodeToUser(DiscountCode|string $discountCode, int $userId): void;
    public function incrementCodeUsedCount(DiscountCode $discountCode, int $amount = 1): bool|int;
    public function getCodeUsers(): LengthAwarePaginator;
}
