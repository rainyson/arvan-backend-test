<?php

namespace App\Interfaces;

use App\Models\User;

interface IUserAccessTokenRepository
{
    public function createUserToken(User $user): string;
}
