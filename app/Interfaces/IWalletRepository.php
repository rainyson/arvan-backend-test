<?php

namespace App\Interfaces;

use App\Models\Wallet;
use Illuminate\Pagination\LengthAwarePaginator;

interface IWalletRepository
{
    public function createWalletTransaction(array $data,int $userId): Wallet;
    public function getListByUser(int $userId): LengthAwarePaginator;
}
