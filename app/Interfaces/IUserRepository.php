<?php

namespace App\Interfaces;

use App\Models\User;

interface IUserRepository
{
    public function findOrCreateByMobile(string $mobile): User;
    public function modifyWallet(User $user, int $amount, int $type = 1): bool|int;
}
