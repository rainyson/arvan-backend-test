<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class DiscountCode extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'creator_id',
        'updater_id',
        'title',
        'code',
        'amount',
        'use_count',
        'used_count',
    ];

    public function creator(): BelongsTo
    {
        return $this->belongsTo(Manager::class, 'creator_id');
    }

    public function updater(): BelongsTo
    {
        return $this->belongsTo(Manager::class, 'updater_id');
    }

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class);
    }
}
