<?php

return [
    'discountCode' => [
        'wrongCode' => 'کد وارد شده نامعتبر است',
        'used' => 'این کد قبلا توسط شما استفاده شده است',
        'useCountExceeded' => 'تعداد استفاده از این کد بیش از حد مجاز است'
    ]
];
